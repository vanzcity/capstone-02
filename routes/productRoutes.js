const express = require('express')
const router = express.Router()
const ProductController = require("../controllers/ProductController")
const auth = require("../auth")


// Create NEW Product
router.post("/create", auth.verifyAdmin, (request, response) => {
	ProductController.createProduct(request.body, request.user).then(result => response.send(result))
})


//Get ALL products
router.get("/", auth.verifyAdmin, (request, response) => {
	ProductController.getAllProducts(request.user).then(result => response.send(result))
})

// Get all ACTIVE products
router.get("/active", (request, response) => {
	ProductController.getAllActive().then(result => response.send(result))
})


// Get single Product
router.get("/:id", (request, response) => {
	ProductController.getProduct(request.params.id).then(result => response.send(result))
})

// Update product information
router.put("/:id/update", auth.verifyAdmin, (request, response) => {
	ProductController.updateInformation(request.params.id, request.body, request.user).then(result => response.send(result))
})

// Archive a product
router.patch("/:id/archive", auth.verifyAdmin, (request, response) => {
	ProductController.archiveProduct(request.params.id, request.user).then(result => response.send(result))
})

// Activate a product
router.put("/:id/update/active", auth.verifyAdmin, (request, response) => {
	ProductController.activateProduct(request.params.id, request.body, request.user).then(result => response.send(result))
})


module.exports = router
