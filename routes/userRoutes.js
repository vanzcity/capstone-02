const express = require("express")
const router = express.Router()
const auth = require("../auth")
const UserController = require('../controllers/UserController')

// Check email for existing user
router.post("/check-email", (request, response) => {
	UserController.checkEmailExists(request.body).then(result => response.send(result))
})


// Register user
router.post('/register', (request, response) => {
	UserController.registerUser(request.body).then(result =>
		response.send(result))
})

//Login User
router.post('/login', (request, response) => {
	UserController.loginUser(request.body).then(result =>
		response.send(result))
})

//Get user details from token
router.get('/details', auth.verify, (request, response) => {
	const user_data = auth.decode(request.headers.authorization)
	UserController.getUserDetails(user_data.id).then(result => 
		response.send(result))
})

// User checkout product
router.post("/checkout", auth.verifyNotAdmin, (request, response) => {
	let request_body = {
		// Getting the userId from the token instead of the request.body property makes sure that the user currently logged in is the user that is going to enroll in a specific course
		user_id: auth.decode(request.headers.authorization).id,
		product_id: request.body.productId,
		productName: request.body.productName,
		quantity: request.body.quantity
	}

	UserController.checkOutOrder(request_body).then(result => response.send(result))
})


// Make user an admin account
router.put("/:id/update/admin", auth.verify, (request, response) => {
	UserController.makeAdmin(request.params.id, request.body).then(result => response.send(result))
})

module.exports = router;