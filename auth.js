const jwt = require("jsonwebtoken")
const secret = "B271ECommerceAPI" //Secret key to be used for validating the token

// Method for generating a token with JWT
module.exports.createAccessToken = (user) => {
	const user_data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(user_data, secret, {})
}

module.exports.verify = (request, response, next) => {
	let token = request.headers.authorization

	if(typeof token !== "undefined"){
		// If the token exists, then slice its first 7 characters to remove the default 'Bearer' text when using the request.headers.authorization property. What will be left is only the token itself.
		token = token.slice(7, token.length)

		// The verify function will check the token and the secret key used in this application to verify where it came from. 

		return jwt.verify(token, secret, (error, data) => {
			if(error){
				return response.send({auth: 'Verification failed.'})
			}

			next()
		})
	} else {
		return response.send({auth: "Token is not defined."})
	}
}


module.exports.decode = (token) => {
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length)

	return jwt.verify(token, secret, (error,data) =>{
		if (error){
			return null
		}

		return jwt.decode(token, {complete: true}).payload

	})
} else {
	return null
}

}

module.exports.verifyAdmin = (request, response, next) => {
	let token = request.headers.authorization

	if(typeof token !== "undefined"){
		// If the token exists, then slice its first 7 characters to remove the default 'Bearer' text when using the request.headers.authorization property. What will be left is only the token itself.
		token = token.slice(7, token.length)

		// The verify function will check the token and the secret key used in this application to verify where it came from. 

		return jwt.verify(token, secret, (error, data) => {
			if(error){
				return response.send({auth: 'Verification failed.'})
			}

			request.user = data;

			if (!data.isAdmin){
				return response.send({auth: 'User is not an admin.'})
			}

			next()
		})
	} else {
		return response.send({auth: "Token is not defined."})
	}
}

module.exports.verifyNotAdmin = (request, response, next) => {
	let token = request.headers.authorization

	if(typeof token !== "undefined"){
		// If the token exists, then slice its first 7 characters to remove the default 'Bearer' text when using the request.headers.authorization property. What will be left is only the token itself.
		token = token.slice(7, token.length)

		// The verify function will check the token and the secret key used in this application to verify where it came from. 

		return jwt.verify(token, secret, (error, data) => {
			if(error){
				return response.send({auth: 'Verification failed.'})
			}

			if (data.isAdmin){
				return response.send({auth: 'Admin cannot checkout a product'})
			}

			next()
		})
	} else {
		return response.send({auth: "Token is not defined."})
	}
}
