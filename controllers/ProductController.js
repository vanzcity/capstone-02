const Product = require("../models/Product");

// Create new product
module.exports.createProduct = (new_content, user) => {
  if (!user.isAdmin) {
    return {
      message: "Only admins can post a new product.",
    };
  }

  let new_product = new Product({
    name: new_content.name,
    description: new_content.description,
    price: new_content.price,
  });

  console.log(new_product)

  return new_product.save().then((created_product, error) => {
    if (error) {
      return error;
    }

    return {
      message: "Added new product successfully!",
      data: created_product,
    };
  });
};

// Get all products
module.exports.getAllProducts = (user) => {
  // Check if the user is an admin
  if (!user.isAdmin) {
    return {
      message: "Only admins can retrieve all products.",
    };
  }

  return Product.find({}).then((products, error) => {
    if (error) {
      return error;
    }

    return products;
  });
};

// Get all ACTIVE products
module.exports.getAllActive = () => {
  return Product.find({ isActive: true }).then((active_products) => {
    return active_products;
  });
};

// Get single product
module.exports.getProduct = (product_id) => {
  return Product.findById(product_id).then((result) => {
    return result;
  });
};

//Update Product Information
module.exports.updateInformation = (product_id, new_content, user) => {
  if (!user.isAdmin) {
    return {
      message: "Only admins can update product information.",
    };
  }

  let updated_product = {
    name: new_content.name,
    description: new_content.description,
    price: new_content.price,
  };

  return Product.findByIdAndUpdate(product_id, updated_product).then(
    (modified_product, error) => {
      if (error) {
        return error;
      }

      return {
        message: "Product information updated successfully!",
        data: modified_product,
      };
    }
  );
};

// Archive Product
module.exports.archiveProduct = (product_id, user) => {
  if (!user.isAdmin) {
    return {
      message: "Only admins can archive products.",
    };
  }

  return Product.findById(product_id).then((product, error) => {
    if (error) {
      return error;
    }

    product.isActive = false;

    return product.save().then((archived_product, error) => {
      if (error) {
        return error;
      }

      return {
        message: "Product archived successfully!",
        data: archived_product,
      };
    });
  });
};

//Activate a product
module.exports.activateProduct = (product_id, new_content) => {
  let activated_product = {
    isActive: new_content.isActive,
  };

  return Product.findByIdAndUpdate(product_id, activated_product).then(
    (active_product, error) => {
      if (error) {
        return error;
      }

      return {
        message: "Product is now active!",
        data: active_product,
      };
    }
  );
};
