const User = require("../models/User");
const Product = require ("../models/Product");
const auth = require ('../auth');
const bcrypt = require('bcrypt');


// Check if email exists
module.exports.checkEmailExists = (request_body) => {
	return User.find({email: request_body.email}).then(result => {
		if(result.length > 0){
			return true
		}

		return false
	})
}


// Register User
module.exports.registerUser = (request_body) => {

  	return User.findOne({ email: request_body.email })
     .then((existingUser) => {
       if (existingUser) {
         // User with the same email already exists, return an appropriate message or throw an error
        return {message: 'User account already exists!'};
      } 

	let new_user = new User({
		email: request_body.email,
		password: bcrypt.hashSync(request_body.password, 10) // hashSync
	})

	return new_user.save().then((registered_user, error) => {
		if(error){
			return error
		}

		return 'User successfully registered!'
	})

})
}


// Login User
module.exports.loginUser = (request_body) => {
	return User.findOne({email: request_body.email}).then(result => {
		if(result === null) {
			return "The user doesn't exist."
		}

		const is_password_correct = bcrypt.compareSync(request_body.password, result.password)

		if (is_password_correct){
			return {
				accessToken: auth.createAccessToken(result.toObject())
			}
		}

		return 'The email and password combination is not correct!'
	})
}


// For checking out a product
module.exports.checkOutOrder = async (request_body) => {
	console.log(request_body)
	let userSaveStatus = await User.findById(request_body.user_id).then(user => {
	if(user.isAdmin !== true) {
	const toBePushed = {
    products: [{
      productID: request_body.product_id,
      productName: request_body.productName,
      quantity: request_body.quantity,
    }]
  }
	console.log('userrr: ', user)
	user.orderedProduct.push(toBePushed)
	console.log('afterpush: ', user)
}
		return user.save().then((user, error) => {
			if(error){
				console.log('error: ', error)
				return false 
			} 

			// Returns true if the orderedProduct field has added a new product inside
			return true 
		})
	})

	let productSaveStatus = await Product.findById(request_body.product_id).then(product => {
		product.userOrders.push({userId: request_body.user_id})

		return product.save().then((product, error) => {
			if(error){
				return false 
			}

			return true 
		})
	})

	// Checks if both the user AND course model have been modified, and returns a string if so.
	if(userSaveStatus && productSaveStatus){
		return {
			message: "Added to cart!"
		}
	} else {
		return {
			message: "Something went wrong."
		}
	}
}

// Retrieve User details
module.exports.getUserDetails = (userId) => {
	return User.findById(userId).then(result => {
		return result
	})
}

//Make user admin
module.exports.makeAdmin = (userId, new_content) => {
	let makeAdmin_user = {
		isAdmin: new_content.isAdmin
	}

	return User.findByIdAndUpdate(userId, makeAdmin_user).then((admin_user, error) => {
		if(error){
			return error
		}

		return {
			message: "This account is now admin!",
			data: admin_user
		}
	})
}



