// Setup dependencies

const express = require("express");
const mongoose = require("mongoose");
const cors = require('cors')
require('dotenv').config() // Initialize ENV
const user_routes = require("./routes/userRoutes")
const product_routes = require('./routes/productRoutes')


// MongoDB Connection
mongoose.connect(`mongodb+srv://admin:admin@cluster0.ox4nlw1.mongodb.net/e-commerce-db1?retryWrites=true&w=majority`);


mongoose.connection.once('open', () => console.log("Connected to MongoDB!"));

// Server Setup
const app = express();

// Middleware
app.use(cors())
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Routes
app.use("/users", user_routes);
app.use('/products', product_routes);

// Server Listening
// To dynamically switch from the port of the localhost to the port of the cloud server, we have to use an OR operator to check which one express.js will use

app.listen(process.env.PORT || 3500, () => {
	console.log(`API is now runnning on port ${process.env.PORT || 3500}`)
})