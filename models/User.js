const mongoose = require("mongoose")

const user_schema = new mongoose.Schema({
	email: {
		type: String,
		required: [true, "Email is required."]
	},

	password: {
		type: String,
		required: [true, "Password is required."]
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	orderedProduct: [ {
			products: [{
				productID: {
					type: String,
					required: [true, "Product ID is required."]
				},

				productName: {
					type: String,
					required: [true, "Product Name is required."]
				},

				quantity: {
					type: Number
				}
			}],

			totalAmount: {
				type: Number
			},

			purchasedOn: {
				type: Date,
				default: new Date()
			}
		}]
})

module.exports = mongoose.model("User", user_schema);